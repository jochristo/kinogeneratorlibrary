﻿namespace KinoGeneratorLibrary.Model
{
    public class Pair
    {
        private int _numberOne;
        private int _numberTwo;
        private int _frequency;
        public Pair()
        {

        }

        public int NumberOne
        {
            get { return this._numberOne; }
            set { this._numberOne = value; }
        }

        public int NumberTwo
        {
            get { return this._numberTwo; }
            set { this._numberTwo = value; }
        }

        public int Frequency
        {
            get { return this._frequency; }
            set { this._frequency = value; }
        }

        public override bool Equals(object obj)
        {
            var pair = obj as Pair;
            return ((this.NumberOne == pair.NumberOne && this.NumberTwo == pair.NumberTwo)
                || (this.NumberOne == pair.NumberTwo && this.NumberTwo == pair.NumberOne));
        }

        public override int GetHashCode()
        {
            int hash = this.NumberOne * 2 + this.NumberTwo ^ 2;
            return hash.GetHashCode();

        }
    }
}
