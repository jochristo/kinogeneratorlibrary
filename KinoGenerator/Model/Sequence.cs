﻿using System.Collections.Generic;

namespace KinoGeneratorLibrary.Model
{
    public class Sequence
    {
        // Holds the list of integers
        private List<int> _Numbers;

        /// <summary>
        /// Constructs a new Sequence object and initializes the numeric list to hold the data.
        /// </summary>
        public Sequence()
        {
            Numbers = new List<int>();
        }

        public List<int> Numbers { get => _Numbers; set => _Numbers = value; }

        /// <summary>
        /// Adds a new integer into the list of numbers.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public bool Add(int number)
        {
            Numbers.Add(number);
            return true;
        }

        public override bool Equals(object obj)
        {
            var sequence = obj as Sequence;
            var numbers = sequence.Numbers;
            var product = 0;
            foreach (var n in numbers)
            {
                var result = n * 2;
                product += result;
            }
            var product2 = 0;
            foreach (var n in this.Numbers)
            {
                var result = n * 2;
                product2 += result;
            }

            return product == product2;
        }

        public override int GetHashCode()
        {
            var numbers = this.Numbers;
            var product = 0;
            foreach (var n in numbers)
            {
                var result = n ^ 2;
                product += result;
            }
            return product.GetHashCode();
        }
    }
}
