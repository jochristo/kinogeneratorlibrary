﻿namespace KinoGeneratorLibrary.Model
{
    /// <summary>
    /// Represents a downloaded sequence from the REST service
    /// Overrides equals/hashing functions to allow duplicate in hashset collections.
    /// </summary>
    public class DownloadedSequence : Sequence
    {
        public DownloadedSequence()
        {

        }

        public override bool Equals(object obj)
        {
            return false;
        }

        public override int GetHashCode()
        {
            return 0;
        }

    }
}
