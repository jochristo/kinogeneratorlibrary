﻿namespace KinoGeneratorLibrary.Model
{
    /// <summary>
    /// Defines the type of the maximum numeric sequence combinations available for generation.
    /// Type 0: Max combinations for all available numbers in the numeric set.
    /// Type 1: Max combinations for already saved(stored) sequences only.
    /// </summary>
    public enum MaxCombinationsType
    {
        AllSavedSequences = 1,
        AllSequences = 0
    }
}
