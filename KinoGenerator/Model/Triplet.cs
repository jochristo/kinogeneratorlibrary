﻿namespace KinoGeneratorLibrary.Model
{
    public class Triplet
    {
        private int _numberOne;
        private int _numberTwo;
        private int _numberThree;
        private int _frequency;
        public Triplet()
        {

        }
        public int NumberOne
        {
            get { return this._numberOne; }
            set { this._numberOne = value; }
        }

        public int NumberTwo
        {
            get { return this._numberTwo; }
            set { this._numberTwo = value; }
        }

        public int NumberThree
        {
            get { return this._numberThree; }
            set { this._numberThree = value; }
        }

        public int Frequency
        {
            get { return this._frequency; }
            set { this._frequency = value; }
        }

        public override bool Equals(object obj)
        {
            var triplet = obj as Triplet;
            return ((this.NumberOne == triplet.NumberOne && this.NumberTwo == triplet.NumberTwo && this.NumberThree == triplet.NumberThree)
                                    || (this.NumberOne == triplet.NumberOne && this.NumberTwo == triplet.NumberThree && this.NumberThree == triplet.NumberTwo)
                                    || (this.NumberOne == triplet.NumberTwo && this.NumberTwo == triplet.NumberOne && this.NumberThree == triplet.NumberThree)
                                    || (this.NumberOne == triplet.NumberTwo && this.NumberTwo == triplet.NumberThree && this.NumberThree == triplet.NumberOne)
                                    || (this.NumberOne == triplet.NumberThree && this.NumberTwo == triplet.NumberOne && this.NumberThree == triplet.NumberTwo)
                                    || (this.NumberOne == triplet.NumberThree && this.NumberTwo == triplet.NumberTwo && this.NumberThree == triplet.NumberOne));
        }

        public override int GetHashCode()
        {
            int hash = this.NumberOne * 2 + this.NumberTwo ^ 2 + this.NumberThree * 100;
            return hash.GetHashCode();

        }
    }
}
