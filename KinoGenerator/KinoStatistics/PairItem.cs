﻿using System.Collections.Generic;

namespace KinoGeneratorLibrary.KinoStatistics
{
    public class PairItem : IStatisticalSequenceType
    {
        private int _size;
        private List<int> _numbers;
        public PairItem()
        {
            _numbers = new List<int>();
        }

        void IStatisticalSequenceType.init()
        {
            this._size = 2;
        }


        public int getSize()
        {
            return this._size;

        }


        public List<int> numbers()
        {
            return this._numbers;
        }
    }
}
