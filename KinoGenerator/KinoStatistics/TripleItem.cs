﻿using System.Collections.Generic;

namespace KinoGeneratorLibrary.KinoStatistics
{
    public class TripletItem : IStatisticalSequenceType
    {
        private int _size;
        private List<int> _numbers;
        public TripletItem(int x, int y, int z)
        {
            _numbers = new List<int>();
            this._numbers.Add(x);
            this._numbers.Add(y);
            this._numbers.Add(z);
        }

        void IStatisticalSequenceType.init()
        {
            this._size = 3;
        }


        public int getSize()
        {
            return this._size;
        }


        public List<int> numbers()
        {
            return this._numbers;
        }
    }
}
