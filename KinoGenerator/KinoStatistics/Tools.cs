﻿using KinoGeneratorLibrary.Model;
using KinoGeneratorLibrary.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KinoGeneratorLibrary.KinoStatistics
{
    public class Tools
    {

        /// <summary>
        /// Calculates the overdues of the numbers in the given sequences for the given set.
        /// </summary>
        /// <param name="sequences"></param>
        /// <param name="set"></param>
        public Dictionary<int, int> Overdues(ICollection<Sequence> sequences, List<int> set)
        {
            var statsSequence = new StatisticalSequence<PairItem>();
            var overdues = new Dictionary<int, int>();
            var counter = 0; // overdues counter
            if (sequences == null)
                throw new ArgumentException("Collections cannot be null.");

            foreach (var s in Set)
            {
                foreach (var seq in sequences)
                {
                    if (!seq.Numbers.Contains(s))
                    {
                        if (s == 1) // first run
                            counter = 1;
                        else
                            counter++;
                    }
                    else
                        counter = 0;
                }
                overdues[s] = counter;
            }
            return overdues;
        }

        protected static List<int> Set
        {
            get
            {
                var set = new List<int>();
                for (var i = 1; i <= 80; i++)
                {
                    set.Add(i);
                }
                return set;
            }
        }

        /// <summary>
        /// Produces a dictionary collection of the numeric pairs and its frequencies found in given collection of sequences
        /// </summary>
        /// <param name="sequences">The input source containing the numeric sequences to get pairs and its frequencies</param>
        /// <returns></returns>
        public static Dictionary<Sequence, int> Pairs(ICollection<Sequence> sequences)
        {
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");
            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer()); // key: sequence, value: frequency

            //pairs stuff
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                //numbers.Sort();
                var count = numbers.Count();
                var limit = count - 2;
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var firstItemPos = row;
                        var firstItem = numbers.ElementAt(firstItemPos);
                        var secondItem = numbers.ElementAt(secondItemPos);
                        var sequence = new Sequence();
                        sequence.Numbers.Add(firstItem);
                        sequence.Numbers.Add(secondItem);

                        var match = sequences.Where(t => t.Numbers.Contains(firstItem) && t.Numbers.Contains(secondItem));
                        if (match != null)
                        {
                            //dictionary.Add(sequence, match.Count());
                            var frequency = match.Count();

                            if (dictionary.ContainsKey(sequence))
                            {
                                dictionary[sequence]++; //up the frequency counter                                
                            }
                            else
                            {
                                dictionary.Add(sequence, 1 + frequency); //new entry
                            }
                        }
                        secondItemPos++; // get next second item
                    }
                    while (secondItemPos < count && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row < limit && numbers != null);
            }

            return dictionary;
        }

        /// <summary>
        /// Produces a dictionary collection of the numeric triplets and its frequencies found in given collection of sequences
        /// </summary>
        /// <param name="sequences">The input source containing the numeric sequences to get pairs and its frequencies</param>
        /// <returns></returns>
        public static Dictionary<Sequence, int> Triplets(ICollection<Sequence> sequences)
        {
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");
            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer()); // key: sequence, value: frequency

            //triplets stuff
            var counter = 0;
            //Stopwatch sw = Stopwatch.StartNew();
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                //numbers.Sort();
                var count = numbers.Count();
                var limit = count - 3; // set max first element position as the third from last position
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var thirdItemPos = secondItemPos + 1;
                        do
                        {
                            var firstItemPos = row;
                            var firstItem = numbers.ElementAt(firstItemPos);
                            var secondItem = numbers.ElementAt(secondItemPos);
                            var thirdItem = numbers.ElementAt(thirdItemPos);
                            var sequence = new Sequence();
                            sequence.Numbers.Add(firstItem);
                            sequence.Numbers.Add(secondItem);
                            sequence.Numbers.Add(thirdItem);


                            var match = sequences.Where(t => t.Numbers.Contains(firstItem) && t.Numbers.Contains(secondItem) && t.Numbers.Contains(thirdItem));
                            if (match != null)
                            {
                                //dictionary.Add(sequence, match.Count());
                                var frequency = match.Count();

                                if (dictionary.ContainsKey(sequence))
                                {
                                    dictionary[sequence]++; //up the frequency counter                                
                                }
                                else
                                {
                                    dictionary.Add(sequence, 1 + frequency); //new entry
                                }
                            }
                            thirdItemPos++; // get next second item
                        }
                        while (thirdItemPos < count - 1);
                        thirdItemPos = 0;
                        secondItemPos++;
                    }
                    while (secondItemPos < count - 2 && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row <= limit && numbers != null);

                counter++;
            }
            //var COUNTER = counter;
            //sw.Stop();
            //var timer = sw.Elapsed.Seconds;            

            return dictionary;
        }

        /// <summary>
        /// Calculates the skips of the numbers in the given sequences for the given set.
        /// </summary>
        /// <param name="sequences"></param>
        /// <param name="set"></param>
        public static Dictionary<int, int> Skips(ICollection<Sequence> sequences)
        {
            var statsSequence = new StatisticalSequence<PairItem>();
            var overdues = new Dictionary<int, int>();
            var counterMap = new Dictionary<int, int>(); // set number, overdue
            for (var i = 1; i <= 80; i++)
            {
                counterMap.Add(i, 0);
            }

            if (sequences == null)
                throw new ArgumentException("Collections cannot be null.");

            var set = new List<int>();
            for (var i = 1; i <= 80; i++)
            {
                set.Add(i);
            }

            foreach (var s in set)
            {
                foreach (var seq in sequences)
                {
                    if (!seq.Numbers.Contains(s))
                    {
                        if (s == 1) // first run
                        {
                            counterMap[s] = 1;
                        }
                        else
                        {
                            counterMap[s]++;
                        }
                    }
                    else
                    {
                        counterMap[s] = 0;
                    }
                }
                overdues[s] = counterMap[s];
            }
            return overdues;
        }

        /// <summary>
        /// Produces a union of sequences in the collection.
        /// </summary>
        /// <param name="sequences"></param>
        /// <returns></returns>
        public static Dictionary<Sequence, int> Union(ICollection<Sequence> sequences)
        {
            if (sequences == null)
                throw new ArgumentException("Collection cannot be null.");
            var dictionary = new Dictionary<Sequence, int>(new SequenceComparer()); // key: sequence, value: frequency


            //triplets stuff
            var counter = 0;
            //Stopwatch sw = Stopwatch.StartNew();
            foreach (var s in sequences)
            {
                var row = 0;
                var numbers = s.Numbers;
                numbers.Sort();
                var count = numbers.Count();
                var limit = count - 3; // set max first element position as the third from last position
                do
                {
                    var secondItemPos = row + 1;
                    do
                    {
                        var thirdItemPos = secondItemPos + 1;
                        do
                        {
                            var firstItemPos = row;
                            var firstItem = numbers.ElementAt(firstItemPos);
                            var secondItem = numbers.ElementAt(secondItemPos);
                            var thirdItem = numbers.ElementAt(thirdItemPos);
                            var sequence = new Sequence();
                            sequence.Numbers.Add(firstItem);
                            sequence.Numbers.Add(secondItem);
                            sequence.Numbers.Add(thirdItem);

                            var match = sequences.Where(t => t.Numbers.Contains(firstItem) && t.Numbers.Contains(secondItem) && t.Numbers.Contains(thirdItem));
                            if (match != null)
                            {
                                //dictionary.Add(sequence, match.Count());
                                var frequency = match.Count();

                                if (dictionary.ContainsKey(sequence))
                                {
                                    dictionary[sequence]++; //up the frequency counter                                
                                }
                                else
                                {
                                    dictionary.Add(sequence, 1 + frequency); //new entry
                                }
                            }
                            thirdItemPos++; // get next second item
                        }
                        while (thirdItemPos < count - 1);
                        thirdItemPos = 0;
                        secondItemPos++;
                    }
                    while (secondItemPos < count - 2 && numbers != null);
                    secondItemPos = 0;
                    row++; // get next row in sequences

                } while (row <= limit && numbers != null);

                counter++;
            }
            //var COUNTER = counter;
            //sw.Stop();
            //var timer = sw.Elapsed.Seconds;            

            return dictionary;
        }
    }
}
