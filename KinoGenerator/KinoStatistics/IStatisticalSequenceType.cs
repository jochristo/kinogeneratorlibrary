﻿using System.Collections.Generic;

namespace KinoGeneratorLibrary.KinoStatistics
{
    public interface IStatisticalSequenceType
    {
        void init();
        int getSize();
        List<int> numbers();
    }
}
