﻿using KinoGeneratorLibrary.Model;
using System.Collections.Generic;
using System.Linq;

namespace KinoGeneratorLibrary.Utilities
{
    /// <summary>
    /// Determines if two KinoGenerator.Sequence objects given are equal.    
    /// </summary>
    public class SequenceComparer : IEqualityComparer<Sequence>
    {
        public SequenceComparer() { }
        public bool Equals(Sequence a, Sequence b)
        {
            if (a != null && b != null)
            {
                var countA = a.Numbers.Count;
                var countB = b.Numbers.Count;
                var isSameSize = countA == countB ? true : false; // determine list contains same number of elements
                if (isSameSize)
                {
                    if (CompareSequence.Sum(a.Numbers) == CompareSequence.Sum(b.Numbers))
                    {
                        var except = a.Numbers.Except(b.Numbers).Count(); // find the set difference if any
                        if (except == 0)
                            return true;
                    }
                }
            }
            return false;
        }

        public int GetHashCode(Sequence obj)
        {
            return base.GetHashCode();
        }
    }
}
