﻿using KinoGeneratorLibrary.Model;
using System.Collections.Generic;

namespace KinoGeneratorLibrary.Utilities
{
    class RefinedPairComparer : IEqualityComparer<Pair>
    {
        public RefinedPairComparer() { }
        public bool Equals(Pair a, Pair b)
        {
            if (a != null && b != null)
            {
                if ((a.NumberOne == b.NumberOne && a.NumberTwo == b.NumberTwo) || (a.NumberOne == b.NumberTwo && a.NumberTwo == b.NumberOne))
                {
                    var countA = a.NumberOne + a.NumberTwo;
                    var countB = b.NumberOne + b.NumberTwo; ;
                    var isSameSize = countA == countB ? true : false; // determine list contains same number of elements
                    return isSameSize;
                }
            }
            return false;
        }

        public int GetHashCode(Pair obj)
        {

            return base.GetHashCode();
        }
    }
}
