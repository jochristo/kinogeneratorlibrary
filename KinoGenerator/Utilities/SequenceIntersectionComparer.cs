﻿using KinoGeneratorLibrary.Model;
using System;
using System.Collections.Generic;

namespace KinoGeneratorLibrary.Utilities
{
    public class SequenceIntersectionComparer
    {
    }

    /// <summary>
    /// Compares two KinoGenerator.Sequence objects. If the sequences are of the same length and 
    /// contain integers that procuce the same sum of values then the two objects are equal.
    /// </summary>
    public static class CompareSequence
    {
        /// <summary>
        /// Compares two KinoGenerator.Sequence object by comparing the count and the sum of the numeric values in their list property.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool IsEqual(Sequence a, Sequence b)
        {
            if (a != null && b != null)
            {
                var countA = a.Numbers.Count;
                var countB = b.Numbers.Count;
                if (countA == countB)
                {
                    if (Sum(a.Numbers) == Sum(b.Numbers))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the sum of values in the collection.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static int Sum(ICollection<int> collection)
        {
            if (collection == null) throw new ArgumentException("Collection cannot be null");
            var sum = 0;
            if (collection != null && collection.Count > 0)
            {
                foreach (var c in collection)
                    sum += c;
            }
            return sum;
        }
    }

}
